----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:28:53 09/30/2016 
-- Design Name: 
-- Module Name:    SDRAM-IDE - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity SDRAM_IDE is
    Port ( A : in  STD_LOGIC_VECTOR (31 downto 0);
           D : inout  STD_LOGIC_VECTOR (3 downto 0);
           SIZ : in  STD_LOGIC_VECTOR (1 downto 0);
           nDSACK : out  STD_LOGIC_VECTOR (1 downto 0);
           ARAM : out  STD_LOGIC_VECTOR (12 downto 0);
           DQ : out  STD_LOGIC_VECTOR (3 downto 0);
           RAS : out  STD_LOGIC;
           CAS : out  STD_LOGIC;
           MEM_WE : out  STD_LOGIC;
           CLK_RAM : out  STD_LOGIC;
           CLK_EN : out  STD_LOGIC;
           BA : out  STD_LOGIC_VECTOR (1 downto 0);
           RW : in  STD_LOGIC;
           nRAM_SEL : out  STD_LOGIC;
           LE_30_RAM : out  STD_LOGIC;
           OE_30_RAM : out  STD_LOGIC;
           LE_RAM_30 : out  STD_LOGIC;
           OE_RAM_30 : out  STD_LOGIC;
           CBREQ : in  STD_LOGIC;
           CBACK : out  STD_LOGIC;
           CIIN : out  STD_LOGIC;
           IDE_CS : out  STD_LOGIC_VECTOR (1 downto 0);
           IDE_A : out  STD_LOGIC_VECTOR (2 downto 0);
           IDE_R : out  STD_LOGIC;
           IDE_W : out  STD_LOGIC;
           IDE_WAIT : in  STD_LOGIC;
           IDE_RESET : out  STD_LOGIC;
			  IDE_BUFFER_DIR : out  STD_LOGIC;
           PLL_C_HALF : in  STD_LOGIC;
           PLL_C : in  STD_LOGIC;
           S : out  STD_LOGIC_VECTOR (1 downto 0);
           ROM_B : out  STD_LOGIC_VECTOR (1 downto 0);
           ROM_EN : out  STD_LOGIC;
           ROM_OE : out  STD_LOGIC;
           ROM_WE : out  STD_LOGIC;
           STERM : out  STD_LOGIC;
           CLK : in  STD_LOGIC;
           nAS : in  STD_LOGIC;
           nDS : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           ECS : in  STD_LOGIC);
end SDRAM_IDE;

architecture Behavioral of SDRAM_IDE is

Function to_std_logic(X: in Boolean) return Std_Logic is
   variable ret : std_logic;
	begin
		if x then 
			ret := '1';  
		else 
			ret := '0'; 
		end if;
   return ret;
end to_std_logic;
	
function MAX(LEFT, RIGHT: INTEGER) return INTEGER is
begin
	if LEFT > RIGHT then 
		return LEFT;
	else 
		return RIGHT;
	end if;
end;


constant NQ_TIMEOUT : integer := 6; --cl2
constant IDE_WAITS : integer := 1;
constant ROM_WAITS : integer := 5;
constant IDE_DELAY : integer := MAX(IDE_WAITS,ROM_WAITS);
	--wait this number of cycles for a refresh
	--should be 60ns minus one cycle, because the refresh command counts too 150mhz= 6,66ns *9 =60ns
	--puls one cycle for safety :(

constant RQ_TIMEOUT : integer := 390;
	--8192 refreshes in 64ms ->8192 refreshes in 3200000 50MHz ticks
	-- -> Refresh after 390 tics ->380 is a safe place to be!


	TYPE sdram_state_machine_type IS (
				powerup, 					
				init_precharge,			
				init_precharge_commit,  
				init_opcode,				
				init_opcode_wait,					
				start_state,				
				refresh_start,				
				refresh_wait,				
				commit_ras,			
				start_cas,			
				commit_cas,			
				data_wait,			
				data_wait2,			
				precharge,			
				precharge_wait			
				);


signal	MY_CYCLE: STD_LOGIC:='0';
signal   IDE_SPACE:STD_LOGIC:='0';
signal   MEM_SPACE:STD_LOGIC:='0';
signal	AUTO_CONFIG:STD_LOGIC:='0';
signal	AUTO_CONFIG_DONE:STD_LOGIC_VECTOR(1 downto 0):="00";
signal	AUTO_CONFIG_PAUSE:STD_LOGIC:='0';
signal	AUTO_CONFIG_DONE_CYCLE:STD_LOGIC_VECTOR(1 downto 0):="00";
signal	SHUT_UP:STD_LOGIC_VECTOR(1 downto 0) :="11";
signal	IDE_BASEADR:STD_LOGIC_VECTOR(7 downto 0) :=x"E9";
signal	MEM_BASE:STD_LOGIC_VECTOR(3 downto 0) :=x"2";
signal	Dout1:STD_LOGIC_VECTOR(3 downto 0):=x"F";
signal	Dout2:STD_LOGIC_VECTOR(3 downto 0):=x"F";
signal	IDE_DSACK_D:STD_LOGIC_VECTOR(IDE_DELAY downto 0):= (others => '1');
signal	DSACK_16BIT:STD_LOGIC :='1';
signal	IDE_ENABLE:STD_LOGIC :='0';
signal	ROM_OE_S:STD_LOGIC :='1';
signal	IDE_R_S:STD_LOGIC :='1';
signal	IDE_W_S:STD_LOGIC :='1';
signal	IDE_BUF_S:STD_LOGIC :='1';
--signal	AUTO_CONFIG_D0:STD_LOGIC;
signal	nAS_D0:STD_LOGIC :='1';
signal	AUTO_CONFIG_FINISH:STD_LOGIC :='0';
signal TRANSFER_IN_PROGRES:STD_LOGIC:= '0';
signal REFRESH: std_logic:= '0';
signal NQ :  STD_LOGIC_VECTOR (3 downto 0):=(others => '0');
signal RQ :  STD_LOGIC_VECTOR (8 downto 0):=(others => '0');
signal CQ :  sdram_state_machine_type:=powerup;  
constant ARAM_OPTCODE: STD_LOGIC_VECTOR (12 downto 0) := "0001000100010"; --cl2
signal ENACLK_PRE : STD_LOGIC :='1';
signal CLK_D0 : STD_LOGIC :='1';
signal STERM_S : STD_LOGIC :='1';
signal CBACK_S : STD_LOGIC :='1';
signal RESET_S : STD_LOGIC :='0';
signal burst_counter : STD_LOGIC_VECTOR(1 downto 0) :="11";
signal RAM_BANK_ACTIVATE  :  STD_LOGIC :='0';
signal LE_RAM_CPU_P : STD_LOGIC :='1';
signal OE_30_RAM_S : STD_LOGIC :='1';
signal OE_RAM_30_S : STD_LOGIC :='1';
 
begin


	--internal signals	
	--output
	MY_CYCLE		<= '0' 	when (IDE_SPACE='1' or AUTO_CONFIG ='1' 
										or MEM_SPACE = '1'
										) else '1';
	nRAM_SEL 	<= MY_CYCLE; 

	--map DSACK signal
	nDSACK		<= "ZZ" when MY_CYCLE ='1' or nAS='1' ELSE
						"01" when DSACK_16BIT	 ='0' else 						
						"01" when AUTO_CONFIG='1' else 
						"11";
	STERM		<= STERM_S when nAS ='0' else '1';
	OE_30_RAM <= OE_30_RAM_S when nAS ='0' else '1';
	OE_RAM_30 <= OE_RAM_30_S when nAS ='0' else '1';
	--enable caching for RAM
	CIIN	<= 'Z' when nAS='1' or (MEM_SPACE ='0' and IDE_SPACE = '0' and AUTO_CONFIG = '0') else
				'1' when MEM_SPACE = '1' else
				'0' ;
	CBACK <= CBACK_S when nAS ='0' else '1';
	
	--PLL control
	
	--very tricky: a 5V device thinks 3,3v = 1 is floating! 
	--So if the pll is a 570A (5V) all 1 must be replaced with Z and all Ms with 1.
	--A 570B (3.3V) behaves as descrived in the datasheet!
		
	--FB CLK/2: SJ2: left
	--FB CLK  : SJ2: right
	
	
	--values for the 570A
	S<="ZZ"; --double the clock - FB is CLK 
	--S<="0Z"; --Quarduple the clock - FB is CLK 
	--S<="01"; --triple the clock - FB is CLK 
	--S<="00"; --disable
	--S<="Z0"; --recover the clock (1x
	--S<="Z0"; --double the clock FB is CLK /2 
	
	--SD-RAM clock-stuff
	CLK_RAM 	<= not PLL_C;
	CLK_EN 	<= ENACLK_PRE;

	--map signals
	--ide stuff
	IDE_CS(0)<= not(A(12));			
	IDE_CS(1)<= not(A(13));
	IDE_A(0)	<= A(9);
	IDE_A(1)	<= A(10);
	IDE_A(2)	<= A(11);
	IDE_BUFFER_DIR	<= IDE_BUF_S when nAS='0' else '1';
	IDE_R		<= IDE_R_S when nAS='0' else '1';
	IDE_W		<= IDE_W_S when nAS='0' else '1';
	IDE_RESET<= RESET;
	ROM_EN	<= IDE_ENABLE;
	ROM_WE	<= '1';
	ROM_OE	<= ROM_OE_S when nAS='0' else '1';
	ROM_B		<= "00";
	
	--data out for autoconfig(tm)
	D	<=	"ZZZZ" when RW='0' or AUTO_CONFIG ='0' or nAS='1' else
			Dout1	when 	AUTO_CONFIG_DONE(0)='0' else
			Dout2;	
	
	--transparent latch for writes
	LE_30_RAM <= '0';


	address_decode:process(RESET_S,nAS)begin
		if(RESET_S='0')then
			IDE_SPACE <= '0';
			AUTO_CONFIG <= '0';
			MEM_SPACE <= '0';	
		elsif(falling_edge(nAS))then
			--IDE address decode section 
			if(A(31 downto 16) = (x"00" & 
						IDE_BASEADR
					) AND SHUT_UP(1) ='0') then
				IDE_SPACE <= '1';
			else
				IDE_SPACE <= '0';
			end if;			
			
			--Autoconfig(tm) address decode section 
			if(A(31 downto 16) =x"00E8" AND AUTO_CONFIG_DONE /="11") then
				AUTO_CONFIG <= '1';
			else
				AUTO_CONFIG <= '0';
			end if;

			--memoy decode
			if(
				(A(31 downto 26) = "000010"
				 and A(25 downto 20) /="111111") or
				(A(31 downto 24) = "00000000"
				 and A(23 downto 20) =MEM_BASE  
				 and SHUT_UP(0)='0' )
				)then
				MEM_SPACE <= '1';			
			else 
				MEM_SPACE <= '0';
			end if;
		        
			
		end if;
	end process address_decode;

	psos_edge_no_reset:process (PLL_C) begin
      if rising_edge(PLL_C) then		
			RESET_S <=RESET;
						--clock edge detection
			CLK_D0	<= CLK;
			
			--nAS delay
			nAS_D0	<=nAS;	
			
		end if;
	end process;

--	--all signals, which need to be clocked on the negative edge
--	neg_edge_ctrl:process(PLL_C)
--	begin
--		if(falling_edge(PLL_C))then
--			--latch control for reads
--			LE_RAM_30<= LE_RAM_CPU_P;
--					
--			
--
--		end if;
--	end process neg_edge_ctrl;
--	
	LE_RAM_30<= LE_RAM_CPU_P;
	--LE_RAM_30<= '0';		
			
		--bussizing decode
	bus_siz_ctrl:process (RAM_BANK_ACTIVATE,PLL_C)begin
		if(RAM_BANK_ACTIVATE = '0' )then
			BA 	<= "00";
			DQ<= "1111"; --high during init
		elsif(rising_edge(PLL_C)) then
			BA <=  A(19 downto 18);
			
			--DQ-mask decoding
			if(RW='0' and CQ=start_cas)then --mask on non long writes
				--now decode the adresslines A[0..1] and SIZ[0..1] to determine the ram bank to write				
				-- bits 0-7
				if(	SIZ="00" or 
						(A(0)='1' and A(1)='1') or 
						(A(1)='1' and SIZ(1)='1') or
						(A(0)='1' and SIZ="11" ))then
					DQ(0)	<= '0';
				else
					DQ(0)	<= '1';
				end if;
						
				-- bits 8-15
				if(	(A(0)='0' and A(1)='1') or
						(A(0)='1' and A(1)='0' and SIZ(0)='0') or
						(A(1)='0' and SIZ="11") or 
						(A(1)='0' and SIZ="00"))then
					DQ(1)<= '0';
				else
					DQ(1)<= '1';
				end if;				
					
				--bits 16-23
				if(	(A(0)='1' and A(1)='0') or
						(A(1)='0' and SIZ(0)='0') or 
						(A(1)='0' and SIZ(1)='1'))then
					DQ(2)	<= '0';
				else
					DQ(2)	<= '1';
				end if;									
					
				--bits 24--31
				if(( 	A(0)='0' and A(1)='0' ))then
					DQ(3)	<= '0';
				else
					DQ(3)	<= '1';
				end if;
			else
				DQ <= "0000"; --all others: full 32 bit
			end if;
		end if;
	end process bus_siz_ctrl;
 
	--all ram signnals, which need to be clocked on the positive edge
	pos_edge_ctrl:process (PLL_C,RESET_S) begin
		if(RESET_S = '0') then
			RAM_BANK_ACTIVATE <='0';
			CQ	<= powerup;
			RAS <= '1';
			CAS <= '1';
			MEM_WE <= '1';
			ENACLK_PRE <= '1';		 
 			ARAM <= (others => '0');
			RQ<=	(others => '0');
			OE_30_RAM_S <= '1';
			OE_RAM_30_S <= '1';
			STERM_S <= '1';
			LE_RAM_CPU_P<= '1';
			TRANSFER_IN_PROGRES <= '0';
			CBACK_S <= '1';
			burst_counter <= "11";
			REFRESH <= '0';
			NQ  <= (others => '0');
      elsif rising_edge(PLL_C) then		
			
			--output buffer control
			if( MEM_SPACE ='1' and nAS='0'
				) then
				OE_30_RAM_S <= RW;
				OE_RAM_30_S <= not RW;
			else
				OE_30_RAM_S <= '1';
				OE_RAM_30_S <= '1';
			end if;

			--sterm control
			if(CQ=commit_cas)then --cl3
				STERM_S <= '0' ;
			elsif(nAS = '1' or CQ=precharge)then
				STERM_S <= '1';
			end if;

			--latch control for reads
			if(	CQ=start_cas or
				   CQ= data_wait2 
				)then 
				LE_RAM_CPU_P<= '0';
			elsif(--CQ=commit_cas or
				   CQ = data_wait			
					)then
				LE_RAM_CPU_P<= '1';
			end if;

			--transfer detection, cacheburst length and cacheburst acknowledge
			if (	(CQ=commit_ras	or TRANSFER_IN_PROGRES = '1') and nAS='0')then
				TRANSFER_IN_PROGRES <= '1';

				--cache burst logic
				if(CBREQ = '0' and TRANSFER_IN_PROGRES ='0' 
					and A(3 downto 2) /= "11")then
					CBACK_S <='0';
					burst_counter <= A(3 downto 2);
				end if;
				
				if(burst_counter = "11" and TRANSFER_IN_PROGRES = '1')then
					CBACK_S <= '1';
				end if;
				
				--burst increment
				if(CQ=data_wait2 and CBACK_S ='0')then
					burst_counter <= burst_counter+1;
				end if;								
			else
				TRANSFER_IN_PROGRES <= '0';
				CBACK_S <= '1';
				burst_counter <= "11";
			end if;
--			CBACK_S <= '1';
--			burst_counter <= "11";

			--refresh flag
			if(CQ = refresh_start)then
				REFRESH <= '0';
			elsif(RQ = RQ_TIMEOUT) then 
				REFRESH <= '1';
			end if;

			--refresh counter
			if (RQ = RQ_TIMEOUT ) then
				RQ<=	(others => '0');
			elsif(CLK_D0 = '1') then --count on edges
				RQ <= RQ + 1;
			end if;
			
			--wait counter stuff
			if(
				--CQ = init_precharge_commit or
				CQ = init_opcode_wait or
				CQ = refresh_wait)
			then
				if(NQ < x"F")then
					NQ <= NQ +1;
				end if;
			else 
				NQ  <= (others => '0');
			end if;				
	
			
			-- default values
			ENACLK_PRE <= '1';		 
			RAS <= '1';
			CAS <= '1';
			MEM_WE <= '1';
			ARAM <= A(17 downto 5);
			
			-- ram state machine decoder
			case CQ is
			when powerup =>
			 CQ <= init_precharge;		
			 ARAM(10) <='1';			 
			when init_precharge =>
			 ARAM(10) <='1';
			 RAS <= '0';
			 MEM_WE <= '0';
			 CQ <= init_precharge_commit;			
			when init_precharge_commit =>
			 ARAM <= ARAM_OPTCODE;			
			 CQ <= init_opcode;  
			when init_opcode =>
			 RAS <= '0';
			 CAS <= '0';
			 MEM_WE <= '0';
			 ARAM <= ARAM_OPTCODE;			
			 CQ <= init_opcode_wait;
			when init_opcode_wait =>
			 if (NQ >= x"1") then
				 CQ <= refresh_start;   --1st refresh
			 else
				 CQ <= init_opcode_wait;
			 end if;	 
			when start_state =>		 
			 if (REFRESH = '1') then
				 CQ <= refresh_start;
			 elsif (	MEM_SPACE ='1' and TRANSFER_IN_PROGRES='0' and nAS='0'
						and CLK_D0='0'
						) then
				RAS <= '0'; -- start ras
				CQ <= commit_ras;
			 else
				CQ <= start_state;
			 end if;			 
			when refresh_start =>
			 RAS <= '0';
			 CAS <= '0';
			 CQ <= refresh_wait;
			when refresh_wait =>
			 if (NQ >= NQ_TIMEOUT) then			--wait 60ns here				 
				 if(RAM_BANK_ACTIVATE ='0')then
					CQ <= refresh_start; --2nd refresh on start up!
				 else
					CQ <= start_state;
				 end if;
				 RAM_BANK_ACTIVATE <='1'; --now its save to switch on the bank decode
			 else
				 CQ <= refresh_wait;
			 end if;
		   when commit_ras =>
			 --mux for ranger/Z2 mem
			 if(A(27)='1')then
				ARAM(8 downto 0) <=  A(25 downto 20) & A(4 downto 2);
			 else
				ARAM(8 downto 0) <= "111111" & A(4 downto 2);
			 end if;	
			 ARAM(10) <= '1'; --with autoprecharge: ARAM(1=)='1'
			 CQ <= start_cas; 
			when start_cas =>
			 --mux for ranger/Z2 mem
			 if(A(27)='1')then
				ARAM(8 downto 0) <=  A(25 downto 20) & A(4 downto 2);
			 else
				ARAM(8 downto 0) <= "111111" & A(4 downto 2);
			 end if;	
			 ARAM(10) <= '1'; --with autoprecharge: ARAM(1=)='1'
			 CAS <= '0';
			 MEM_WE <= RW;
			 CQ <= commit_cas;
			when commit_cas =>
			 MEM_WE <= not CBACK_S or not RW; --burst stop if no cache burst
			 CQ <= data_wait;
			 ENACLK_PRE <= CBACK_S; --delay comes one clock later!			 
			when data_wait => 
			 --ARAM(10) <='1';
			 ENACLK_PRE <= CBACK_S; --delay comes one clock later!
			 if(CBACK_S = '1')then
				CQ <= precharge;
			 else
				CQ <= data_wait2;
			 end if;
			when data_wait2 =>
			 --ENACLK_PRE <= '0';
			 ARAM(10) <='1';
			 if(burst_counter = "11")then
				CQ <= precharge;
			 else
				CQ <= data_wait;
			 end if;
			 --CQ <= data_wait;						 
			when precharge =>
			 ARAM(10) <='1';
			 RAS <= '0';
			 MEM_WE <= '0';
			 CQ <= precharge_wait;
			when precharge_wait =>
			 CQ <= start_state; 
			end case;
		end if;
   end process pos_edge_ctrl;	


	ac_ctrl:process (PLL_C,RESET_S) begin
		if(RESET_S = '0') then
			AUTO_CONFIG_PAUSE <= '0';
			AUTO_CONFIG_DONE_CYCLE	<= "00";
			AUTO_CONFIG_DONE	<= "00";
			
			--use these presets for CDTV: This makes the DMAC config first!
			--AUTO_CONFIG_PAUSE <='1';
			--AUTO_CONFIG_DONE_CYCLE	<='1';
			--AUTO_CONFIG_DONE	<='1';
			Dout1 <= "1111";
			Dout2 <= "1111";
			SHUT_UP	<= "11";
			IDE_BASEADR <= x"E9";
			MEM_BASE <= x"2";

			--AUTO_CONFIG_D0 <= '0';
      elsif rising_edge(PLL_C) then		

			--Autoconfig(tm) data-encoding

--				if( 	A(31 downto 16) = x"00E8" 
--						and A (6 downto 1)= "100100"
--						and RW='0' and nAS_D0='0')  then
--					AUTO_CONFIG_FINISH <= '1';
--				else
--					AUTO_CONFIG_FINISH <= '0';
--				end if;
				
--				-- wait one autoconfig-strobe for CDTV!
--				if(AUTO_CONFIG_FINISH = '1'
--					and nAS_D0='1' and AUTO_CONFIG_PAUSE ='1') then
--					AUTO_CONFIG_PAUSE <= '0';
--					AUTO_CONFIG_DONE_CYCLE	<= "00";
--					AUTO_CONFIG_DONE <= "00";
--				els
			if(nAS= '1' and nAS_D0= '0' )then
				AUTO_CONFIG_DONE <= AUTO_CONFIG_DONE or AUTO_CONFIG_DONE_CYCLE;
			end if;
		
			Dout1 <=	"1111" ;
			Dout2 <=	"1111" ;
			if(AUTO_CONFIG = '1' and nAS = '0') then
				--AUTO_CONFIG_D0 <= '1';
				case A(6 downto 1) is
					when "000000"	=> 
						--Dout1 <= "1110" ; --ZII, Memory,  no ROM
						--Dout2 <= "1101" ; --ZII, no Memory,  ROM
						--Dout1(1) <=	'0' ; --activate this line for a memtest system!
						Dout1(0) <=	'0' ;
						Dout2(1) <=	'0' ;
					when "000001"	=> 
						--Dout1 <=	"0101" ; --one Card, 1MB =  101
						--Dout2 <=	"0001" ; --one Card, 64kb = 001
						Dout1(1) <=	'0' ;
						Dout1(3) <=	'0' ;
						Dout2(3 downto 1) <=	"000" ;
					when "000011"	=> 
						--Dout1 <=	"1001" ;
						--Dout2 <=	"1001" ; --ProductID low nibble: 9->0110=6
						Dout1(2 downto 1) <=	"00" ;
						Dout2(2 downto 1) <=	"00" ;
					when "001001"	=> 
						--Dout1 <=	"0101" ;
						--Dout2 <=	"0111" ; --Ventor ID 1
						Dout1(1) <=	'0' ;
						Dout1(3) <=	'0' ;
						Dout2(3) <=	'0' ;
					when "001010"	=> 
						--Dout1 <=	"1110" ;
						--Dout2 <=	"1101" ; --Ventor ID 2
						Dout1(0) <=	'0' ;
						Dout2(1) <=	'0' ;
						
					when "001011"	=> 
						--Dout1 <=	"0011" ; --Ventor ID 3 : $0A1C: A1k.org
						--Dout2 <=	"0011" ; --Ventor ID 3 : $082C: BSC
						Dout1(3 downto 2) <=	"00" ;
						Dout2(3 downto 2) <=	"00" ;
					when "001100"	=> 
						--Dout1 <=	"1111" ;
						--Dout2 <=	"0100" ; --Serial byte 0 (msb) high nibble
						Dout2(0) <=	'0' ;
						Dout2(1) <=	'0' ;
						Dout2(3) <=	'0' ;
					when "001101"	=> 
						--Dout1 <=	"1111" ;
						--Dout2 <=	"1110" ; --Serial byte 0 (msb) low  nibble
						Dout2(0) <=	'0' ;
					when "001110"	=> 
						--Dout1 <=	"1111" ;
						--Dout2 <=	"1001" ; --Serial byte 1       high nibble
						Dout2(2 downto 1) <=	"00" ;
					when "001111"	=> 
						--Dout1 <=	"1111" ;
						--Dout2 <=	"0100" ; --Serial byte 1       low  nibble
						Dout2(0) <=	'0' ;
						Dout2(1) <=	'0' ;
						Dout2(3) <=	'0' ;
					when "010010"	=> 
						--Dout1 <=	"1111" ;
						--Dout2 <=	"0100" ; --Serial byte 3 (lsb) high nibble
						Dout2(0) <=	'0' ;
						Dout2(1) <=	'0' ;
						Dout2(3) <=	'0' ;
					when "010011"	=> 
						--Dout1 <=	"1111" ;
						--Dout2 <=	"1010" ; --Serial byte 3 (lsb) low  nibble: B16B00B5
						Dout2(0) <=	'0' ;
						Dout2(2) <=	'0' ;
					when "010111"	=> 
						--Dout1 <=	"1111" ;
						--Dout2 <=	"1110" ; --Rom vector low byte low  nibble
						Dout2(0) <=	'0' ; --Rom vector low byte low  nibble
					when "100100"	=> 
						if(nDS = '0' and RW='0' and AUTO_CONFIG_DONE(0) = '0')then 
							MEM_BASE <= D(3 downto 0);
							SHUT_UP(0) <= '0'; --enable board
							AUTO_CONFIG_DONE_CYCLE(0)	<= '1'; --done here
						end if;
						if(nDS = '0' and RW='0' and AUTO_CONFIG_DONE(0)  = '1')then 
							IDE_BASEADR(7 downto 4)	<= D(3 downto 0); --Base adress
							SHUT_UP(1) <= '0'; --enable board
							AUTO_CONFIG_DONE_CYCLE(1)	<= '1'; --done here
						end if;
					when "100101"	=> 
						if(nDS = '0' and RW='0' and AUTO_CONFIG_DONE(0) = '1')then 
							IDE_BASEADR(3 downto 0)	<= D(3 downto 0); --Base adress
						end if;
					when "100110"	=> 
						if(nDS = '0' and RW='0' and AUTO_CONFIG_DONE(0) = '0')then 
							AUTO_CONFIG_DONE_CYCLE(0)	<= '1'; --done here
						end if;
						if(nDS = '0' and RW='0' and AUTO_CONFIG_DONE(0) = '1')then 
							AUTO_CONFIG_DONE_CYCLE(1)	<= '1'; --done here
						end if;
					when others	=> 
						--Dout1 <=	"1111" ;
						--Dout2 <=	"1111" ;
				end case;	
			end if;
		end if;
   end process ac_ctrl;	


	
	-- this is the clocked ide-process
	ide_rw_gen: process (RESET_S,CLK)
	begin
		if(RESET_S = '0')then
			IDE_ENABLE			<='0';
			IDE_BUF_S <= '1';
			IDE_R_S		<= '1';
			IDE_W_S		<= '1';
			ROM_OE_S	<= '1';
			IDE_DSACK_D		<= (others => '1');
			DSACK_16BIT			<= '1';		

		elsif rising_edge(CLK) then
		
			if(IDE_SPACE = '1' and nAS = '0' and RW = '0')then
				--enable IDE on the first write on this IO-space!
				IDE_ENABLE <= '1';
			end if;							
			
			--default values
			IDE_BUF_S <= '1';
			IDE_R_S		<= '1';
			IDE_W_S		<= '1';
			ROM_OE_S	<= '1';
			IDE_DSACK_D		<= (others => '1');
			DSACK_16BIT			<= '1';		
		
			if(IDE_SPACE = '1' and nAS = '0')then
				IDE_BUF_S <= not RW;
				if(RW = '0')then
					--the write goes to the hdd!
					IDE_W_S		<= '0';
					if(IDE_WAIT = '1')then --IDE I/O
						DSACK_16BIT		<=	IDE_DSACK_D(IDE_WAITS);
					end if;
				elsif(RW = '1' and IDE_ENABLE = '1')then
					--read from IDE instead from ROM
					IDE_R_S		<= '0';
					if(IDE_WAIT = '1')then --IDE I/O
						DSACK_16BIT		<=	IDE_DSACK_D(IDE_WAITS);
					end if;
				elsif(RW = '1' and IDE_ENABLE = '0')then
					DSACK_16BIT		<= IDE_DSACK_D(ROM_WAITS);
					ROM_OE_S		<=	'0';	
				end if;
		
				IDE_DSACK_D(0)		<=	'0';
				IDE_DSACK_D(IDE_DELAY downto 1) <= IDE_DSACK_D((IDE_DELAY-1) downto 0);
			end if;				
		end if;
	end process ide_rw_gen;

end Behavioral;

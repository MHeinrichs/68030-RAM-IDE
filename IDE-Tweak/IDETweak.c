/*
 * MapROM utility for Matze`s 68020 CPU card
 *

 *
 */

#include <exec/exec.h>
#include <exec/execbase.h>
#include <dos/dos.h>
#include <exec/libraries.h>
#include <libraries/expansion.h>
#include <libraries/expansionbase.h>
#include <proto/expansion.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "idetweak_rev.h"


/* Global vars */

BOOL quiet = FALSE;
BOOL force = FALSE;

const char *version = VERSTAG;

/* ReadArgs stuff */

#define CMD_TEMPLATE	"STATUS/S,WAITSTATE/N"
#define ARG_STATUS	0
#define ARG_WAITSTATE	1
#define ARG_COUNT	2
#define MAX_WAIT 0x000E // maximum allowed wait states

#define MANUFACTOR 2092
#define PRODUCT 6
#define SERIAL 0xB16B00B5 //dicke ???
#define IDE_CONFIG_OFFSET 0x6000 // A15+A14 is the switch! A15+A14 = 0x3000*2 (word-pointer multiplicator)



/* Function prototypes */


void WriteWaits(UWORD val);
UWORD ReadWaits(void);
ULONG* LocateIDEBoard(void);


/* The fun starts here */

int main(int argc, char *argv[]) {
	struct RDArgs *args;
	LONG *argvalues[ARG_COUNT] = { NULL };
	UWORD waitState = 0, waitStateRead = 0;

	/*** Commandline parser ***/

	if( (args = AllocDosObject(DOS_RDARGS, NULL)) ) {
		if( (ReadArgs(CMD_TEMPLATE, (LONG *) argvalues, args)) ) {


			if(argvalues[ARG_STATUS]) {
				waitStateRead = ReadWaits();
				if(waitStateRead<=MAX_WAIT){
					printf("IDE waitstates: %d\n",waitStateRead);
				}
				else{
					printf("Board not found! %d\n",waitStateRead);					
				}
			}


			if(argvalues[ARG_WAITSTATE]){
				waitState = *argvalues[ARG_WAITSTATE];

				WriteWaits(waitState);
			}


			
			FreeArgs(args);
		}

		FreeDosObject(DOS_RDARGS, args);
	}	

	return(0);
}


/*
 * WriteWaits - Write IDE waitstates
 *
 * Parameters: -
 *
 * Returns:    -
 *
 */
void WriteWaits(UWORD map) {
	UWORD *enable = (UWORD*)LocateIDEBoard(); //cast to uword because we have to write to D31-D28 only once!
	if(!enable)
		return;	
	enable+=IDE_CONFIG_OFFSET;
	map = map & MAX_WAIT; //allow only last 4 bits
	map = map<<12; //shift 12 bit to put the values to D31-D16
	*enable = map;
}


/*
 * ReadWaits - read ide waitstate setting
 *
 * Parameters: -
 *
 * Returns:    the parameters from the map rom register
 *
 */
UWORD ReadWaits(void) {
	UWORD retVal=0;
	UWORD *enable = (UWORD*)LocateIDEBoard(); //cast to uword because we have to write to D31-D28 only once!
	if(!enable)
		return -1;	
	
	enable+=IDE_CONFIG_OFFSET;
	retVal = *enable;
	retVal = retVal >>12;
	retVal = retVal & MAX_WAIT; //mask unset bits
	return retVal;
}


/*
** **********************************************
** LocateIDEBoard()
** **********************************************
**
** Locates IDE Controller hardware
**
*/
ULONG* LocateIDEBoard(void)
{
    ULONG* board= NULL; // Board not found (yet)
    struct Library *ExpansionBase= NULL;
 

    if ( ExpansionBase = OpenLibrary( "expansion.library", 0L ) ) {

        struct ConfigDev *cfg;

        // *** Find IDE board

        if (cfg = FindConfigDev( NULL, MANUFACTOR, PRODUCT )) 
		{
			//find correct serial number
			if(cfg->cd_Rom.er_SerialNumber==SERIAL){ 
				board = cfg->cd_BoardAddr; // Get board base adr.
			}
		}

        CloseLibrary( ExpansionBase );
    }

    return( board ); // Return board ptr (or NULL)
}
